<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne as HasOne;

class User extends Model
{

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'identifier';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return BelongsToMany
     */
    public function webUser()
    {

        return $this->belongsToMany(WebUser::class);
    }

    /**
     * @return HasOne
     */
    public function whitelistApplication()
    {

        return $this->hasOne(WhitelistApplications::class);
    }

    /**
     * @return HasMany
     */
    public function criminalRecords()
    {

        return $this->hasMany(CriminalRecord::class);
    }

    /**
     * @return HasMany
     */
    public function medicalHistory()
    {

        return $this->hasMany(MedicalHistory::class);
    }

    /**
     * @return HasMany
     */
    public function wantedCharacter()
    {

        return $this->hasMany(WantedCharacters::class);
    }

    /**
     * @return HasMany
     */
    public function jobApplication()
    {

        return $this->hasMany(JobApplication::class);
    }

    /**
     * @return HasOne
     */
    public function policeOfficer()
    {

        return $this->hasOne(PoliceOfficer::class);
    }

    /**
     * @return HasOne
     */
    public function emsWorker()
    {

        return $this->hasOne(EmsWorker::class);
    }

    /**
     * @return HasMany
     */
    public function mechanicReports()
    {

        return $this->hasMany(MechanicReports::class);
    }
}
