<?php
namespace App\Helpers;

/**
* Helper to manipulate data from bills
*/
class BillsHelper
{

    /**
     * @param mixed $sociatyBills
     * @param int $allBillsCount
     * @return int|float
     */
    public static function getPrecentOf($sociatyBills, $allBillsCount)
    {

        if(!$allBillsCount or !$sociatyBills){
            return 0;
        }
        $percent = $sociatyBills->count() / $allBillsCount;
        return number_format( $percent * 100, 2 );
    }
}
