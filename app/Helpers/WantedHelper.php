<?php

namespace App\Helpers;

use App\WantedCharacters;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BanHelper
 */
class WantedHelper
{

    /**
     * @param string $characterName
     * @param Collection $wanted
     * @return bool
     */
    public static function isWantedByCharacter($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param WantedCharacters $wantedCharacters
     * @return bool
     */
    public static function isUserWanted($wantedCharacters)
    {

        if ($wantedCharacters->count() > 0) {
            return true;
        }
        return false;
    }

    /**
     * @param $characterName
     * @param Collection $wanted
     * @return int|null
     */
    public static function getWantedId($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {
                return $item->id;
            }
        }

        return null;
    }

    /**
     * @param $characterName
     * @param Collection $wanted
     * @return int|null
     */
    public static function getWantedReason($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {
                return $item->reason;
            }
        }

        return null;
    }

    /**
     * @param $characterName
     * @param Collection $wanted
     * @return |null
     */
    public static function getCreatorName($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {
                return $item->creator_name;
            }
        }

        return null;
    }

    /**
     * @param $characterName
     * @param Collection $wanted
     * @return |null
     */
    public static function getCreationTime($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {
                return $item->created_at;
            }
        }

        return null;
    }/**
     * @param $characterName
     * @param Collection $wanted
     * @return |null
     */
    public static function getImageName($characterName, Collection $wanted)
    {

        foreach ($wanted as $item) {
            if (StringHelper::trimString($characterName) ===StringHelper::trimString($item->character_name)) {

                if(isset($item->image)) {
                    return $item->image;
                }
            }
        }

        return null;
    }

    /**
     * Get all wanted characters
     *
     * @return WantedCharacters[]|Collection
     */
    public static function getAllWantedCharacters()
    {

        return WantedCharacters::all();
    }
}
