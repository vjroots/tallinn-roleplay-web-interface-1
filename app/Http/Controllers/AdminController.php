<?php

namespace App\Http\Controllers;
use App\Bill;
use App\Character;
use App\CharacterKillLog;
use App\CriminalRecord;
use App\Helpers\Sql\CharacterKillHelper;
use App\Helpers\Sql\FivemUserHelper;
use App\MedicalHistory;
use App\Motel;
use App\OwnedCar;
use App\OwnedMotel;
use App\OwnedProperty;
use App\Property;
use App\User;
use App\UserLicenses;
use App\WantedCharacters;
use App\Whitelist;
use App\WhitelistApplications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * Class AdminController
 */
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->middleware('auth');
    }

    /**
     * Show list of all fivem users
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allUsers(Request $request)
    {

        $request->user()->authorizeRoles('admin');
        return view(
            'common.alluser',
            [
                'users' => User::paginate(10),
                'searchRoute' => 'adminSearchUser',
                'searchPlaceholder' => __('forms.search_user'),
                'singleUserRoute' => 'adminSingleUser',
                'oldSearch' => $request->search
            ]
        );
    }

    /**
     * Show single fivem user info and actions
     *
     * @param Request $request
     * @param $steamId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function singleUser(Request $request, $steamId)
    {
        $request->user()->authorizeRoles('admin');

        $ownedProperties = [];
        foreach (OwnedProperty::where('owner', $steamId)->get() as $ownedProperty) {
            $property = new \stdClass();
            $property->name = $ownedProperty->name;
            $property->isRented = $ownedProperty->rented;
            $property->label = Property::where('name', $ownedProperty->name)->first()->label;
            $ownedProperties[] = $property;
        }

        foreach (OwnedMotel::where('owner', $steamId)->get() as $ownedMotel) {
            $property = new \stdClass();
            $property->name = $ownedMotel->name;
            $property->isRented = true;
            $property->label = Motel::where('name', $ownedMotel->name)->first()->label;
            $ownedProperties[] = $property;
        }

        $croppedSteamId = str_replace('steam:', '', $steamId);
        $whiteList = Whitelist::where('identifier', 'like', '%' . $croppedSteamId . '%')->first();

        $user = User::find($steamId);
        return view(
            'admin.user',
            [
                'user' => $user,
                'bills' => Bill::where('identifier', $steamId)->get(),
                'licenses' => UserLicenses::where('owner', '=', $steamId)->get(),
                'ownedProperties' => $ownedProperties,
                'criminalRecords' => $user->criminalRecords()->orderBy('created_at', 'DESC')->get(),
                'characters' => Character::where('identifier', $steamId)->get(),
                'wanted' => $user->wantedCharacter()->get(),
                'medicalHistory' => $user->medicalHistory()->orderBy('created_at', 'DESC')->get(),
                'cars' => OwnedCar::where('owner', $steamId)->get(),
                'isWhitelisted' => ($whiteList) ? true : false,
                'isWhitelistBanned' => ($whiteList and $whiteList->identifier !== $steamId) ? true : false,
                'isCkCandidate' => $this->isUserCkCandidate($steamId)
            ]
        );
    }

    /**
     * Show search results of users
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function searchUser(Request $request)
    {

        $request->user()->authorizeRoles('admin');
        return view(
            'common.alluser',
            [
                'users' => FivemUserHelper::searchUsers($request->input('search')),
                'searchRoute' => 'adminSearchUser',
                'searchPlaceholder' => 'search user',
                'singleUserRoute' => 'adminSingleUser'
            ]
        );
    }

    public function characterKill(Request $request)
    {

        $request->user()->authorizeRoles('admin');

        $user = User::find($request->identifier);

        CharacterKillHelper::removeAddOnAccountData($user->identifier);
        CharacterKillHelper::removeAddOnInventoryItems($user->identifier);
        CharacterKillHelper::removeBills($user->identifier);
        CharacterKillHelper::removeCreatedBills($user->identifier);
        CharacterKillHelper::removeCharacters($user->identifier);
        CharacterKillHelper::removeCriminalRecords($user->identifier);
        CharacterKillHelper::removeDataStoreData($user->identifier);
        CharacterKillHelper::removeMedicalHistory($user->identifier);
        CharacterKillHelper::removeOwnedDock($user->identifier);
        CharacterKillHelper::removeOwnedProperties($user->identifier);
        CharacterKillHelper::removeOwnedVehicles($user->identifier);
        CharacterKillHelper::removePhoneCalls($user->phone_number);
        CharacterKillHelper::removePhoneMessages($user->phone_number);
        CharacterKillHelper::removePhoneUserContacts($user->identifier, $user->phone_number);
        CharacterKillHelper::removePlayersTattoos($user->identifier);
        CharacterKillHelper::removeUserAccounts($user->identifier);
        CharacterKillHelper::removeUserInventory($user->identifier);
        CharacterKillHelper::removeUserLicenses($user->identifier);
        CharacterKillHelper::removeWantedCharacters($user->identifier);
        CharacterKillHelper::removeCreatedCriminalRecords($user->identifier);
        CharacterKillHelper::removeCreatedMedicalRecords($user->identifier);
        CharacterKillHelper::removeCarSellHistory($user->identifier);
        CharacterKillHelper::removeUsers($user->identifier);

        $loggedInUserData = Auth::user()->getFiveMUserData();
        $log = new CharacterKillLog();
        $log->adminName = Auth::user()->name;
        $log->adminIdentifier = $loggedInUserData->identifier;
        $log->userName = $user->name;
        $log->userIdentifier = $user->identifier;
        $log->save();

        return redirect(route('adminAllUsers'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function banFromWhitelist(Request $request)
    {

        $request->user()->authorizeRoles('admin');

        $croppedSteamId = str_replace('steam:', '', $request->identifier);
        $whitelist = Whitelist::where('identifier', 'like', '%' . $croppedSteamId . '%')->first();

        if ($whitelist and strpos($whitelist->identifier, 'steam') !== false) {
            $newIdentifier = substr_replace($request->identifier, 'ban', 0, 5);
            DB::table('whitelist')
                ->where('identifier', $whitelist->identifier)
                ->update(['identifier' => $newIdentifier]);
        } elseif($whitelist and strpos($whitelist->identifier, 'ban') !== false) {
            $whitelist->identifier = $request->identifier;
            DB::table('whitelist')
                ->where('identifier', 'like', '%' . $croppedSteamId .'%')
                ->update(['identifier' => $request->identifier]);
        }

        return redirect()->back()->with('status', 'Whitelist Ban status changed');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showWhitelistApplications(Request $request)
    {

        $request->user()->authorizeRoles('admin');

        return view(
            'admin.whitelistApplications',
            ['whitelists' => WhitelistApplications::where('isAccepted', false)->where('isRejected', false)->get()]
            );
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rejectWhitelistApplication(Request $request)
    {

        $request->user()->authorizeRoles('admin');

        $whitelistApplication = WhitelistApplications::find($request->id);
        $whitelistApplication->isRejected = true;
        $whitelistApplication->isAccepted = false;
        $whitelistApplication->rejectionReason = $request->reason;
        $whitelistApplication->save();

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function acceptWhitelistApplication(Request $request)
    {

        $request->user()->authorizeRoles('admin');

        $whitelistApplication = WhitelistApplications::find($request->id);
        $whitelistApplication->isRejected = false;
        $whitelistApplication->isAccepted = true;
        $whitelistApplication->save();

        $whitelist = new Whitelist();
        $whitelist->identifier = $whitelistApplication->user_identifier;
        $whitelist->save();

        return redirect()->back();
    }

    /**
     * CK candidate is user who has been jail over 300h according to criminal records
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Exception
     */
    public function ckCandidates()
    {

        $criminalRecords = CriminalRecord::whereNotNull('jail_time')
            ->where('jail_time', '>', 0)
            ->whereDate('created_at', '>', (new \DateTime())->modify('-1 month'))
            ->orderBy('user_identifier', 'DESC')
            ->get(['user_identifier']);
        $identifierCollection = collect($criminalRecords->toArray())->unique('user_identifier');

        $ckCandidates = collect();
        foreach ($identifierCollection as $identifier) {
           if($this->isUserCkCandidate($identifier['user_identifier'])) {
               $ckCandidates->push($identifier['user_identifier']);
           }
        }

        return view('common.alluser',
            [
                'users' => User::whereIn('identifier', $ckCandidates)->get(),
                'searchRoute' => 'adminSearchUser',
                'searchPlaceholder' => __('forms.search_user'),
                'singleUserRoute' => 'adminSingleUser'
            ]
        );
    }

    private function isUserCkCandidate($identifier)
    {

        try {
            $jailTimes = collect(CriminalRecord::where('user_identifier', $identifier)
                ->whereNotNull('jail_time')
                ->where('jail_time', '>', 0)
                ->whereDate('created_at', '>', (new \DateTime())->modify('-1 month'))
                ->get(['jail_time'])
                ->toArray());
            if ($jailTimes->sum('jail_time') > 299) {
                return true;
            }

            return false;
        } catch (\Exception $exception) {
            echo 'Data is corrupted for ' . $identifier;
            dd($jailTimes);
        }
    }
}
