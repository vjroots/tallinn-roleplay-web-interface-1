<?php
namespace App\Http\Controllers\Auth;

use App\Helpers\Sql\FivemUserHelper;
use App\Http\Controllers\Controller;
use App\Role;
use App\WebUser;
use App\Whitelist;
use App\WhitelistApplications;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class WhitelistController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {

        return view('whitelist.application');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     * @throws \Exception
     */
    public function store(Request $request)
    {

        request()->validate([

            'name' => 'required|min:2|max:50',

            'email' => 'required|email|unique:web_users',

            'password' => 'required|min:6|confirmed',

            'characterName' => 'required|string|min:2|max:50',

            'steamName' => 'required|string|min:2|max:20',

            'hex' => 'required|string|min:2|max:50'
        ], [

            'name.required' => __('wlValidationErrors.name_required'),

            'name.min' => __('wlValidationErrors.name_min'),

            'name.max' => __('wlValidationErrors.name_max'),

            'characterName.required' => __('wlValidationErrors.character_required'),

            'characterName.min' => __('wlValidationErrors.character_min'),

            'characterName.max' => __('wlValidationErrors.character_max'),

            'steamName.required' => __('wlValidationErrors.steam_required'),

            'steamName.min' => __('wlValidationErrors.steam_min'),

            'steamName.max' => __('wlValidationErrors.steam_max'),

        ]);

        $steamId = 'steam:' . strtolower($request->hex);

        if (Whitelist::where('identifier', $steamId)->orWhere('identifier', 'ban:' . $request->hex)->first()) {
            return redirect()->back()->withInput($request->all())->withErrors(['hex' => __('wlValidationErrors.already_whitelisted')]);
        }

        if(FivemUserHelper::searchUsers($request->characterName)->count() > 0) {
            return redirect()->back()->withInput($request->all())->withErrors(['characterName' => __('wlValidationErrors.character_name_exists')]);
        }

        $user = WebUser::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => Hash::make(\request('password')),
        ]);
        $user
            ->roles()
            ->attach(Role::where('name', 'player')->first());

        // List of extra questions names
        $extraQuestionsList = [
            'whenYouCanUseWeapons',
            'newLifeRule',
            'helmetMaskRules',
            'robbingOtherCitizen',
            'carTax',
            'vdmRdm',
            'oocIc',
            'radioUsage',
        ];

        $extraQuestionsAndAnswers = [];
        foreach ($request->all() as $question => $answer) {
            if (in_array($question, $extraQuestionsList)) {
                $extraQuestionsAndAnswers[$question] = $answer;
            }
        }

        $application = new WhitelistApplications();
        $application->user_identifier = $steamId;
        $application->steamName = $request->steamName;
        $application->characterName = $request-> characterName;
        $application->isAccepted = false;
        $application->isRejected = false;
        $application->web_user_id = $user->id;
        $application->extra_questions = json_encode($extraQuestionsAndAnswers);
        $application->save();

        Auth::loginUsingId($user->id);
        return redirect(route('whitelistApplicationStatus'));
    }
}
