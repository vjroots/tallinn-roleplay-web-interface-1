<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Motel extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'motell';
}
