@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.searchBar', ['routeName' => 'ambulanceSearchUser', 'placeholder' => 'search user'])
        @endcomponent
    </div>
    @component('components.usersTable', ['routeName' => 'ambulanceSingleUser', 'users' => $users, 'wanted' => (isset($wanted)) ? $wanted : null])
    @endcomponent
    @if(method_exists($users, 'links'))
    <div class="container">
        <div class="pagination justify-content-center p-4">
            {{$users->links()}}
        </div>
    </div>
    @endif
@endsection
