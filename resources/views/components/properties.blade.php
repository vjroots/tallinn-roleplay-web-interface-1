<div class="card-header mouse-over" data-toggle="collapse"
     href="#propertiesBody"
     aria-expanded="false"
     aria-controls="propertiesBody">
    <b>{{__('headers.properties')}}</b>
    <span class="badge badge-pill badge-secondary">{{\App\OwnedProperty::where('owner', $identifier)->get()->count()}}</span>
</div>
<div class="card-body collapse" id="propertiesBody">
    <ul>
        @foreach($ownedProperties as $ownedProperty)
            <div class="border-bottom border-dark d-block">
                <p><b>{{__('texts.name')}}:</b> {{$ownedProperty->label}}</p>
                <p><b>{{__('texts.is_rented')}}:</b> {{$ownedProperty->isRented ? __('texts.true') : __('texts.false')}}</p>
            </div>
        @endforeach
    </ul>
</div>
