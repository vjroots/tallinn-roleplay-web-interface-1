@if(isset($collapse) and $collapse)
    <div class="card-header mouse-over" data-toggle="collapse"
         href="#billsBody"
         aria-expanded="false"
         aria-controls="billsBody">
 @else
     <div class="card-header">
 @endif
    <b>
        {{__('headers.bills')}}
    </b>
    <span class="badge badge-pill badge-secondary">{{\App\Bill::where('identifier', $identifier)->get()->count()}}</span>
</div>
    @if(isset($collapse) and $collapse)
<div class="card-body collapse" id="billsBody">
@else
    <div class="card-body" id="billsBody">
@endif
    <table class="table">
        <thead class="thead-light">
        <tr>
            <th scope="col">{{__('tables.label')}}</th>
            <th scope="col">{{__('tables.issuer')}}</th>
            <th scope="col">{{__('tables.receive')}}</th>
            <th scope="col">{{__('tables.amount')}}</th>
            @if(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police')
                <th scope="col">{{__('tables.actions')}}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($bills as $bill)
            @if(!isset($restrict) or !$restrict)
            <tr>
                <td scope="row">{{$bill->label}}</td>
                <td>{{\App\User::find($bill->sender)->name}}</td>
                <td>{{$bill->target}}</td>
                <td>{{$bill->amount}}</td>
            </tr>
            @elseif($restrict and $bill->target === $societyTpe)
                <tr>
                    <td>{{$bill->label}}</td>
                    <td>{{\App\User::find($bill->sender)->name}}</td>
                    <td>{{$bill->target}}</td>
                    <td>{{$bill->amount}}</td>
                    @if(\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police')
                        <td>
                            <form method="POST" action="{{route('policeDeleteBill')}}">
                                @csrf
                                <input type="hidden" name="billId" value="{{$bill->id}}">
                                <input type="hidden" name="billOwner" value="{{$bill->identifier}}">
                                <button type="submit" class="btn btn-danger" onclick="overlayOn()">{{__('buttons.delete')}}</button>
                            </form>
                        </td>
                    @endif
                </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>
