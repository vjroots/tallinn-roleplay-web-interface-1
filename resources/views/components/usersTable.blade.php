<table class="table">
    <thead class="thead-light">
    <tr>
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
            <th scope="col">{{__('tables.steam_name')}}</th>
        @endif
        <th scope="col">{{__('tables.character_name')}}</th>
        <th scope="col">{{__('tables.date_of_birth')}}</th>
        <th scope="col">{{__('tables.sex')}}</th>
        @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
            <th scope="col">Group</th>
            <th scope="col">Group Level</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @foreach($users as $user)
        @php
            $isWanted = false;
        @endphp

        {{-- search only wanted stuff, if wanted info is sent to this view component --}}
        @if(isset($wanted))
            @foreach($wanted as $wantedInfo)
                @if($user->identifier === $wantedInfo->criminal_steam_id)
                    @php
                        $isWanted = true;
                    @endphp
                @endif
            @endforeach
        @endif
        @if($isWanted)
            <tr class="alert-danger mouse-over"
                onclick="
                    window.location='{{route($routeName, [$user->identifier])}}';
                    overlayOn();
                    ">
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <th scope="row">{{ $user->name }}</th>
                @endif
                <td>{{ \App\Helpers\UserHelper::getCharacterName($user) }}</td>
                <td>{{ $user->dateofbirth }}</td>
                <td>{{ $user->sex }}</td>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <td>{{ $user->group }}</td>
                    <td>{{ $user->permission_level }}</td>
                @endif
            </tr>
        @else
            <tr class="mouse-over"
                onclick="
                    window.location='{{route($routeName, [$user->identifier])}}';
                    overlayOn();
                    ">
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <td>{{ $user->name }}</td>
                @endif
                <td>{{ \App\Helpers\UserHelper::getCharacterName($user) }}</td>
                <td>{{ $user->dateofbirth }}</td>
                <td>{{ $user->sex }}</td>
                @if(\Illuminate\Support\Facades\Auth::user()->hasRole('admin'))
                    <td>{{ $user->group }}</td>
                    <td>{{ $user->permission_level }}</td>
                @endif
            </tr>
        @endif
    @endforeach
    </tbody>
</table>
