@extends('help.layout.layout')
@section('helpHeader')
    <strong>Üldine info</strong>
@endsection
@section('helpBody')
    <div>
        <h4><strong>Key binds / Nupud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                    <li><strong>F1</strong> - Põhi menüü mille kaudu saab avada telefoni, inventuuri jne</li>
                    <li><strong>F2</strong> - Lemmik animatsioon (ennem tuleb lemmik animatsioon valida)</li>
                    <li><strong>F2</strong> - Sulgeb inventuuri kui see on avatud</li>
                    <li><strong>F3</strong> - Animatsioonid</li>
                    <li><strong>F6</strong> - Töö menüü</li>
                    <li><strong>F9</strong> - Lemmiklooma menüü</li>
                    <li><strong>F12</strong> - Pildistamine (steami funktsionaalsus)</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>E</strong> - Vilkurid (politsei, kiirabi ja mehaanikute sõidukitel)</li>
                    <li><strong>T</strong> - Text chat</li>
                    <li><strong>Y</strong> - Püsikiiruse hoidja</li>
                    <li><strong>G</strong> - Sireenid (politsei ja kiirabi sõidukitel)</li>
                    <li><strong>H</strong> - Hääle/kuulmise kauguse muutmine.</li>
                    <li><strong>Z</strong> - Turvavöö</li>
                    <li><strong>X</strong> - Käed ülesse</li>
                    <li><strong>C</strong> - Selja taha vaatamine ja narkootikumide müümine</li>
                    <li><strong>B</strong> - Näita näpuga</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <li><strong>NUM 7</strong></li>
                    <li><strong>SHIFT + L</strong> - Käed raudu koos animatsiooniga (ainult politseil)</li>
                    <li><strong>HOME</strong> - Mängijate nimekiri</li>
                    <li><strong>CTRL</strong> - kükitamine</li>
                    <li><strong>CAPS LOCK</strong> - Raadio Saatjaga ja telefoniga rääkimine (push to talk)</li>
                    <li><strong>ESC / backspace</strong> - Menüüde sulgemine</li>
                </ul>
            </div>
        </div>
        <h4><strong>Commandid / Käsud</strong></h4>
        <div class="row">
            <div class="col-md-4">
                <ul>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                </ul>
            </div>
        </div>
    </div>
@endsection
