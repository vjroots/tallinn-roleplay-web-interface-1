@extends('help.layout.layout')
@section('helpHeader')
    <strong>Politsei Reeglid</strong>
@endsection
@section('helpBody')
    <div>
        <h4 id="Üldreeglid"><strong>Üldreeglid</strong></h4>
        <ol>
            <li>Allud endast kõrgemale ametikohale!</li>
            <li>Korduvate vigadega on õigus juhatajal määrata disiplinaar karistus!</li>
            <li>ERARIIETES töö tegemine + niisama on-dudy istumine keelatud!</li>
            <li>Peale igat tööpäeva ON KOHUSTUS KÕIK relvad panna tagasi relvakappi</li>
            <li>OFF-DUDY minnes paned relvad relvakappi ära, need ei kuulu sulle vaid asutusele</li>
            <li>Relva kaotamisel annad sellest koheselt teada oma otsesele ülemusele</li>
            <li>Vilkurite põhjendamatu sisse lülitamine KEELATUD!</li>
        </ol>

        <h4 id="Rööv"><strong>Rööv</strong></h4>
        <ol>
            <li>Enne südnmuskohale minekut paned selga kuulivesti ning valmistad ette vajalikud relvad jaoskonnas</li>
            <li>Kogunemine röövile reageerimiseks toimub politsei jaoskonnas või kokkulepitud kohas leitnandi või kõrgema auastmega isiku käsul</li>
            <li>Sündmuskohale jõudes EI PARGI otse ukse ette vaid hoiad piisavat DISTANTS</li>
            <li>Politsei TULD ilma asjata ei ava! (Ainult juhul kui pätt on sinu vastu tule avanud ning olukord seda soodustab)</li>
        </ol>
    </div>
@endsection
