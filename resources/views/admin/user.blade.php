@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="container">
                @component('components.searchBar', ['routeName' => 'adminSearchUser', 'placeholder' => 'search user'])
                @endcomponent
            </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{__('headers.user_data')}}</div>

                    <div class="card-body">

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (isset($isCkCandidate) && $isCkCandidate)
                            <div class="alert alert-danger" role="alert">
                                {{strtoupper('user have jail time more then 300 minutes')}}
                            </div>
                        @endif
                        {{-- ADMIN ACTIONS--}}
                        <div class="card-header"><b>{{__('headers.actions')}}</b></div>
                        <div class="card-body">
                            <form method="POST" action="{{route('adminCharacterKill')}}" class="d-inline-block"
                                  onsubmit="return ConfirmDelete()">
                                @csrf
                                <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                <button type="submit" class="btn btn-danger" onclick="overlayOn()">
                                    {{__('buttons.delete_user')}}
                                </button>
                            </form>
                            @if($isWhitelisted)
                                <form method="POST" action="{{route('adminBanFromWhitelist')}}" class="d-inline-block"
                                      onsubmit="return ConfirmDelete()">
                                    @csrf
                                    <input type="hidden" name="identifier" value="{{$user->identifier}}">
                                    @if($isWhitelistBanned)
                                        <button type="submit" class="btn btn-warning" onclick="overlayOn()">
                                            {{__('buttons.un_ban_user_from_whitelist')}}
                                        </button>
                                    @else
                                        <button type="submit" class="btn btn-warning" onclick="overlayOn()">
                                            {{__('buttons.ban_user_from_whitelist')}}
                                        </button>
                                    @endif
                                </form>
                            @endif
                        </div>
                        @component('components.userData', ['userData' => $user])
                        @endcomponent
                        @component('components.bills', ['bills' => $bills,  'identifier' => $user->identifier])
                        @endcomponent
                        @component('components.licenses', ['licenses' => $licenses, 'identifier' => $user->identifier])
                        @endcomponent
                        @component('components.properties', ['ownedProperties' => $ownedProperties, 'identifier' => $user->identifier])
                        @endcomponent
                        @component('police.components.userCars', ['cars' => $cars,  'owner' => $user, 'collapse' => true])
                        @endcomponent
                        @component(
                            'components.criminalRecordsTable',
                            [
                                'characters' => $characters,
                                'criminalRecords' => $criminalRecords,
                                'actions' => false,
                                'criminalIdentifier' => $user->identifier
                            ]
                        )
                        @endcomponent
                        @component('components.medicalHistoryTable', ['characters' => $characters, 'medicalHistory' => $medicalHistory, 'actions' => false, 'identifier' => $user->identifier])
                        @endcomponent
                        @component('components.characters', ['characters' => $characters, 'actions' => false, 'police' => false, 'identifier' => $user->identifier])
                        @endcomponent
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }

</script>
