@extends('layouts.app')

@section('content')
    <div class="container">
        @component('components.searchBar', ['routeName' => 'policeSearchUser', 'placeholder' => __('forms.search_user')])
        @endcomponent
        <h2 class="text-gray">Criminal Record #{{$criminalRecord->id}}</h2>
        <div class="card mt-5">
        <div class="card-body">
        @if($criminalRecord->is_criminal_case and new DateTime($criminalRecord->criminal_case_expires_at) > new DateTime())
            <div class="alert alert-danger">Active criminal record</div>
        @endif
            <p onclick="
                    window.location='{{route('policeSingleUser', [$criminalRecord->user_identifier])}}';
                    overlayOn();"
                class="mouse-over"
            >
                <b>Name:</b> {{$criminalRecord->character_name}}
        </p>
        <p>
            <b>Fines:</b>

            @if(isset($criminalRecord->fine))
                @foreach(json_decode($criminalRecord->fine) as $fine)
                    <p>{{$fine->label}} {{$fine->amount}}€</p>
                @endforeach
            @endif
            </p>
            <p><b>Jail Time:</b> {{$criminalRecord->jail_time}}</p>
            <p><b>Warnings:</b>
            <ul>
                @if($criminalRecord->warning_car)
                    <li>Car</li>@endif
                @if($criminalRecord->warning_driver_license)
                    <li>Driver License</li>@endif
                @if($criminalRecord->warning_gun_license)
                    <li>Gun License</li>@endif
            </ul>
            </p>
            <p><b>Criminal Case: </b>{{($criminalRecord->is_criminal_case) ? 'Yes' : 'No'}}</p>
            @if($criminalRecord->is_criminal_case)
                <p><b>Expires at: </b>{{$criminalRecord->criminal_case_expires_at}}</p>
            @endif
            <p><b>Summary:</b> {{$criminalRecord->summary}}</p>
            <p><b>Issuer:</b>
                {{\App\Helpers\UserHelper::getCharacterName($criminalRecord->policeOfficer()->first()->user()->first())}}
            </p>
            <a class="btn btn-info d-inline-block" href="{{URL::previous()}}" onclick="overlayOn()">
                {{__('buttons.back_to_previous_page')}}
            </a>
            @if(
                 (\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police'
                 and (
                         \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->identifier === $criminalRecord->officer_id
                         or in_array(
                             \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job_grade,
                             \App\Http\Controllers\PoliceController::ALLOWED_JOB_GRADES)
                     )
                 )
                 or \Illuminate\Support\Facades\Auth::user()->hasRole('admin')
             )
                <form method="POST" action="{{route('policeDeleteReport')}}" onsubmit="return ConfirmDelete()"
                      class="d-inline-block">
                    @csrf
                    <input type="hidden" name="criminalRecordId" value="{{$criminalRecord->id}}">
                    <input type="hidden" name="loggedInOfficerId"
                           value="{{\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->identifier}}">
                    <button type="submit" class="btn btn-danger" onclick="overlayOn()">
                        {{__('buttons.delete')}}
                    </button>
                </form>
            @endif
            @if(
                (\Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job === 'police'
                and (
                        \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->identifier === $criminalRecord->officer_id
                        or in_array(
                            \Illuminate\Support\Facades\Auth::user()->getFiveMUserData()->job_grade,
                            \App\Http\Controllers\PoliceController::ALLOWED_JOB_GRADES)
                    )
                )
                or \Illuminate\Support\Facades\Auth::user()->hasRole('admin')
            )
                <a href="{{route('showEditCriminalRecord', ['id' => $criminalRecord->id])}}" class="btn btn-info">
                    {{__('buttons.edit')}}
                </a>
            @endif
            </div>
        </div>
    </div>
@endsection
<script>

    /**
     *
     * @returns {boolean}
     * @constructor
     */
    function ConfirmDelete() {
        var x = confirm("Are you sure you want to delete?");
        if (x)
            return true;
        else
            return false;
    }

</script>
