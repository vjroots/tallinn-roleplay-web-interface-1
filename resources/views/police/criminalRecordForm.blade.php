@extends('layouts.app')

@section('content')
    <div class="container">
        <h2 class="text-gray">
            {{__('headers.fill_criminal_record_for')}}
            {{\App\Helpers\UserHelper::getCharacterName(\App\User::find($criminalIdentifier))}}
        </h2>
        @if ($errors->any())
            <div class="alert alert-danger mt-4">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="card mt-4">
        <div class="card-body">
        <form method="POST"
            @if(isset($criminalRecord))
                action="{{route('updateCriminalRecord')}}"
            @else
                action="{{route('addCriminalRecord')}}"
            @endif
        >
            @csrf
            <input type="hidden" name="officerId" value="{{$officerIdentifier}}">
            <input type="hidden" name="criminalId" value="{{$criminalIdentifier}}">
            @isset($criminalRecord)
                <input type="hidden" name="recordId" value="{{$criminalRecord->id}}">
            @endisset
            <div class="form-row">
                <div class="form-group col-md-6 mt-4">
                    <p><b>{{__('texts.active_fines')}}</b></p>
                    @foreach($bills as $bill)
                        @if($bill->target === 'society_police')
                            <p>{{$bill->label}} {{$bill->amount}}</p>
                        @endif
                    @endforeach
                </div>
                <div class="form-group col-md-6 mt-4">
                    <p><b>{{__('texts.jail_time_according_to_fines')}}</b></p>
                    <p>{{__('texts.minimum')}}: {{$minJail}}</p>
                    <p>{{__('texts.maximum')}}: {{$maxJail}}</p>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label for="jailTime">{{__('forms.jail_time')}}</label>
                    <input id="jailTime" name="jailTime" type="number" class="form-control"
                           @if(isset($criminalRecord))
                           value="{{$criminalRecord->jail_time}}"
                           @else
                           value="0"
                        @endif
                    >
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <p class="font-weight-bold">{{__('texts.warnings')}}</p>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="car" name="warningCar"
                               @if(isset($criminalRecord) and $criminalRecord->warning_car)
                               checked
                            @endif
                        >
                        <label class="form-check-label" for="car">
                            {{__('forms.make_warning_about_car')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="driverLicense" name="warningDriverLicense"
                               @if(isset($criminalRecord) and $criminalRecord->warning_driver_license)
                               checked
                            @endif
                        >
                        <label class="form-check-label" for="driverLicence">
                            {{__('forms.make_warning_about_traffic_violation_drives_license')}}
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gunLicense" name="warningGunLicense"
                               @if(isset($criminalRecord) and $criminalRecord->warning_gun_license)
                               checked
                            @endif
                        >
                        <label class="form-check-label" for="gunLicense">
                            {{__('forms.make_warning_about_gun_law_violation_gun_licence')}}
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-6">
                    <p class="font-weight-bold">{{__('texts.is_this_criminal_case')}}</p>
                    <input type="radio" id="criminalCaseTrue" name="criminalCase" value="{{true}}"
                           @if(isset($criminalRecord) and $criminalRecord->is_criminal_case)
                           checked
                        @endif
                    >
                    <label for="criminalCaseTrue">{{__('forms.it_s_criminal_case')}}</label><br>
                    <input type="radio" id="criminalCaseFalse" name="criminalCase" value="{{false}}"
                           @if(isset($criminalRecord) and !$criminalRecord->is_criminal_case)
                           checked
                        @endif
                    >
                    <label for="criminalCaseFalse">{{__('forms.it_s_not_criminal_case')}}</label><br>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="summary">{{__('forms.summary')}}</label>
                    <textarea class="form-control" id="summary" name="summary" rows="4"
                              required>@isset($criminalRecord){{trim($criminalRecord->summary, "\x00..\x1F")}}@endisset</textarea>
                </div>
            </div>
            @if(isset($criminalRecord))
                <button class="btn btn-info" id="confirmButton" onclick="overlayOn()" disabled>{{__('buttons.edit_record')}}</button>
            @else
                <button class="btn btn-info" id="confirmButton" onclick="overlayOn()" disabled>{{__('buttons.create_record')}}</button>
            @endif
        </form>
        </div>
        </div>
    </div>
@endsection

<script>
    window.onload = function () {
        const summaryField = document.getElementById('summary');
        const jailTimeField = document.getElementById('jailTime');
        const confirmButton = document.getElementById('confirmButton');
        let elementArray = [
            summaryField,
            jailTimeField
        ];

        elementArray.forEach(function (element) {
            element.addEventListener('keyup', function (event) {
                let isValidSummary = summaryField.checkValidity();
                let isValidJailTimeField = jailTimeField.checkValidity();

                if (isValidSummary && isValidJailTimeField) {
                    confirmButton.disabled = false;
                } else {
                    confirmButton.disabled = true;
                }
            });
        });
    }
</script>
