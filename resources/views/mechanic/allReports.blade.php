@extends('mechanic.layout.layout')
@section('mechanicBody')
    <div class="card">
        <div class="card-header justify-content-center"><b>{{__('headers.mechanic_my_report')}}</b></div>

        <div class="card-body">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th scope="col">{{__('tables.job_type')}}</th>
                        <th>{{__('tables.job_price')}}</th>
                        <th>{{__('tables.number_plate')}}</th>
                        <th>{{__('tables.service_type')}}</th>
                        <th>{{__('tables.summary')}}</th>
                        <th>{{__('tables.worker')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($reports as $report)
                        <tr>
                            <td>{{ __('forms.' . \App\Http\Controllers\MechanicController::JOB_TYPE[$report->job_type]) }}</td>
                            <td>{{ $report->job_price }}</td>
                            <td>{{ $report->car_plate }}</td>
                            <td>{{  __('forms.' . \App\Http\Controllers\MechanicController::SERVICE_TYPE[$report->service_type]) }}</td>
                            <td>{{ $report->summary }}</td>
                            <td>{{ $report->user->firstname }} {{ $report->user->lastname }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @if(method_exists($reports, 'links'))
                <div class="container">
                    <div class="pagination justify-content-center p-4">
                        {{$reports->links()}}
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection

<script>
window.onload = function () {
    const jobPrice = document.getElementById('jobPrice');
    const confirmButton = document.getElementById('confirmButton');

    jobPrice.addEventListener('keyup', function (event) {
        let isValidJobPrice = jobPrice.checkValidity();

        if (isValidJobPrice) {
            confirmButton.disabled = false;
        } else {
            confirmButton.disabled = true;
        }
    });
}
</script>
