@extends('user.layout.layout')
@section('userBody')
    <div class="row row-eq-height mt-5 d-flex" style="height: 160px">
        <div class="col-md-6 mouse-over h-100" onclick="showProfileUpload()">
            @component('components.userDataBox', ['user' => $userData, 'profilePicture' => $profilePicture])
            @endcomponent
        </div>
        <div class="col-md-6 h-100">
            @component('components.moneyBox', ['data' => $userData])
            @endcomponent
        </div>
    </div>
    <div class="row row-eq-height mt-5 d-flex" style="height: 250px;">
        <div class="col-md-6">
          @component('components.billsBox', ['billsExtraData' => $billsExtraData, 'bills' => $bills, 'userData' => $userData])
          @endcomponent
        </div>
    </div>
    <div class="card mt-5">
        @component('components.licenses', ['licenses' => $licenses, 'identifier' => $userData->identifier])
        @endcomponent
        @component('components.properties', ['ownedProperties' => $ownedProperties, 'identifier' => $userData->identifier])
        @endcomponent
    </div>
@endsection
<div class="overlay" id="profileUpdate">
    <div class="container center">
        <div class="row justify-content-md-center">
            <div class="card col-md-6 mouse-over-regular justify-content-md-center">
                <div class="card-header text-center font-weight-bold">
                    {{__('headers.update_profile')}}
                    <i class="far fa-times-circle fa-2x float-md-right mouse-over" style="color: #fe7985;" onclick="hideProfileUpload()"></i>
                </div>
                <div class="card-body align-content-md-center justify-content-md-center text-center">
                    <form method="POST" action="{{route('homeUploadProfilePicture')}}" enctype="multipart/form-data" onsubmit="overlayOn()"
                    class="align-content-md-center justify-content-md-center text-center">
                        @csrf
                        <label for="carImage" class="mt-2">{{__('forms.profile_picture')}}</label>
                        <input type="file" name="image" id="carImage" class="d-block btn-dark btn col-md-12">
                        <button type="submit" class="btn btn-info mt-3"
                                id="confirmButtonWanted">
                            {{__('buttons.confirm')}}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>

    /**
     * @returns {boolean}
     * @constructor
     */
    function ConfirmSell() {
        var x = confirm("Are you sure you want to continue?");
        if (x)
            return true;
        else
            return false;
    }
</script>
