<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'e_mail_address' => 'E-Maili Aadress',
    'password' => 'Parool',
    'confirm_password' => 'Parooli Kinnitus',
    'remember_me' => 'Jää sisse logituks',
    'buyer_name' => 'Ostja Nimi',
    'name' => 'Nimi',
    'character_name' => 'Karakteri nimi',
    'steam_name' => 'Steam-i Nimi',
    'steam_hex' => 'Steam-i Hex',
    'jail_time' => 'Vanglakaristuse Pikkus',
    'make_warning_about_car' => 'Tee hoiatus auto kohta',
    'make_warning_about_traffic_violation_drives_license' => 'Tee hoiatus liiklusrikkumise kohta',
    'make_warning_about_gun_law_violation_gun_licence' => 'Tee hoatus relvaseaduse rikkumise kohta',
    'it_s_criminal_case' => 'Juthum on kriminaalne',
    'it_s_not_criminal_case' => 'Juhtum ei ole kriminaalne',
    'summary' => 'Kokkuvõte',
    'profile_picture' => 'Passi Pilt',
    'search_user' => 'Otsi kasutajat',
    'search_car' => 'Otsi autosid numbrimärgi järgi',
    'search_fine' => 'Otsi trahve',
    'search_records' => 'Otsi kirjeid id, karistatud isiku nime või kirje loonud politseiniku nime järgi',
    'search_car_sell_request' => 'Otsi auto müügi avalust auto numbrimärgi või müügi avalduse numbri järgi',
    'car_model_name' => 'Auto Mudel Nimi',
    'reason' => 'Põhjus',
    'picture' => 'Pilt',
    'description' => 'Kirjeldus',
    'mechanic_job_type' => 'Töö Tüüp',
    'mechanic_car_trasnport' => 'Autode Transport',
    'mechanic_car_fix' => 'Auto Parandus',
    'mechanic_car_tuning' => 'Auto Tuunimine',
    'mechanic_car_deals' => 'Sõidukitega seotud tehingud',
    'mechanic_job_price' => 'Töö Hind',
    'mechainc_npc' => 'Kohalik',
    'mechainc_citizen' => 'Kodanik',
    'mechainc_ambulance' => 'Kiirabi',
    'mechainc_police' => 'Politsei',
    'mechainc_call' => 'Politsei Väljakutse',
    'car_plate' => 'Auto numbrimärk',
    'whenYouCanUseWeapons' => 'Millises olukorras võite kasutada relva?',
    'newLifeRule' => 'Mis juhtub Teiega kui olete haavatud ning jooksete verest tühjaks, ehk siis kiirabi teid ei elusta?',
    'helmetMaskRules' => 'Kirjelda millal on lubatud kasutada kiivrit ja millal maski?',
    'robbingOtherCitizen' => 'Millisel hetkel on lubatud röövida teist mängijat, mis ketkel NPCd?',
    'carTax' => 'Millal on kohustus tasuda automaksu?',
    'vdmRdm' => 'Seleta lahti oma sõnadega VDM, RDM, Cop Bait',
    'oocIc' => 'Seleta lahti OOC, IC',
    'radioUsage' => 'Kellel on lubatud kasutada raadiot (discordi)?',
];
