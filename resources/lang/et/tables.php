<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */

    'label'=> 'Nimetus',
    'issuer'=> 'Väljastaja',
    'receive'=> 'Saaja',
    'amount'=> 'Summa',
    'actions'=> 'Tegevused',
    'character_name' => 'Karakteri Nimi',
    'date_of_birth' => 'Sünniaeg',
    'is_wanted' => 'Tagaotsitav',
    'wanted_reason' => 'Tagaotsimise Põhjus',
    'declared_wanted_by' => 'Tagaotsitavaks Kuulutaja',
    'date' => 'Kuupäev',
    'picture' => 'Pilt',
    'number_plate' => 'Numbri Märk',
    'model_name' => 'Mudeli Nimi',
    'is_insured' => 'Kindlustus',
    'is_stolen' => 'Varastatus',
    'insurance_end_date' => 'Kindlustuse Lõpp Kuupäev',
    'sell_car' => 'Auto müük',
    'sell_request_nr' => 'Müügi avalduse Nr.',
    'jail_time' => 'Vangla karistuse pikkus',
    'summary' => 'Kokkuvõte',
    'is_criminal_case' => 'Kriminaalasi',
    'created_at' => 'Loodud',
    'criminal_case_expires_at' => 'Krimiasja aegumis kuupäev',
    'owner_steam_name' => 'Omaniku Steami Nimi',
    'owner_name' => 'Omaniku Nimi',
    'officer_name' => 'Politsei Nimi',
    'description' => 'Kirjeldus',
    'category' => 'Kategooria',
    'min_jail' => 'Min Vangistus',
    'max_jail' => 'Max Vangistus',
    'steam_name' => 'Steam-i Nimi',
    'job' => 'Töö',
    'sex' => 'Sugu',
    'request_nr' => 'Avalduse Nr.',
    'seller' => 'Seller',
    'buyer' => 'Buyer',
    'approver' => 'Kinnitaja',
    'name' => 'Nimi',
    'model' => 'Mudel',
    'price' => 'Hind',
    'last_rejection_reason' => 'Viimase Keeldumise Põhjus',
    'steam_id' => 'Steam id',
    'job_type' => 'Töö tüüp',
    'job_price' => 'Töö hind',
    'service_type' => 'Teenust osutati',
    'worker' => 'Töötaja',
    'com_service_min' => 'Min ÜKT',
    'com_service_max' => 'Max ÜKT',
];
