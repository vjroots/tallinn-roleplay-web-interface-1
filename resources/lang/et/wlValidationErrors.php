<?php

return [
    'name_required' => 'Name is required',
    'name_min' => 'Name must be at least 2 characters',
    'name_max' => 'Name should not be greater than 50 characters',
    'character_required' => 'Character name is required',
    'character_min' => 'Character name must be at least 2 characters',
    'character_max' => 'Character name should not be greater than 50 characters',
    'steam_required' => 'Steam name is required',
    'steam_min' => 'Steam name must be at least 2 characters',
    'steam_max' => 'Steam name should not be greater than 20 characters',
];
