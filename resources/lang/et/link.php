<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Links
    |--------------------------------------------------------------------------
    |
    | This file contains translations of links
    |
    */

    'home' => 'Minu Andmed',
    'help' => 'Reeglid ja Info',
    'job_applications' => 'Töö Avaldused',
    'accepted_job_applications' => 'Vastu Võetud Töö Avaldused',
    'police_application' => 'Politsei Avaldus',
    'ambulance_application' => 'Kiirabi Avaldus',
    'police' => 'Politsei',
    'players_list' => 'Mängijate Nimekiri',
    'cars_list' => 'Autode Nimekiri',
    'reports_list' => 'Raportite Nimekiri',
    'fines_list' => 'Trahvide Nimekiri',
    'officers_list' => 'Politseinike Nimekiri',
    'ambulance' => 'Kiirabi',
    'workers_list' => 'Töötajate Nimekiri',
    'car_dealer' => 'ARK',
    'used_cars_sell_request_list' => 'Kasutatud Autode Müügi Avaldused',
    'used_cars_sell_history' => 'Kasutatud Autode Müügi Ajalugu',
    'all_available_vehicles_marks' => 'Kõik Saada Olevad Sõidukid',
    'all_owned_cars' => 'Kõik Omatud Autod',
    'admin' => 'Admin',
    'all_users' => 'Kõik Kasutajad',
    'whitelist_applications' => 'Whitelist Avaldused',
    'ck_candidates' => 'CK Kandidaadid',
    'logout' => 'Logi Välja',
    'login' => 'Logi Sisse',
    'whitelist_application' => 'Whitelist Avaldus',
    'my_data' => 'Minu Andmed',
    'my_car' => 'Minu Autod',
    'criminal_records' => 'Karistusregister',
    'medical_records' => 'Meditsiinilised andmed',
    'my_Job_applications' => 'Minu Töö Avaldused',
    'user_data' => 'Kasutaja Andmed',
    'user_cars' => 'Kasutaja Autod',
    'forgot_your_password' => 'Unustasid Parooli?',
    'server_rules' => 'Serveri Reeglid',
    'police_rules' => 'Politsei Reeglid',
    'ems_rules' => 'Kiirabi Reeglid',
    'mechanic' => 'Mehaanik',
    'new_report' => 'Uus sissekanne',
    'my_reports' => 'Minu sissekanded',
    'all_mechanic_reports' => 'Kõik sissekanded',
    'server_info' => 'Info',
    'radio' => 'Raadio'
];
