<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */
    'user_data' => 'Kasutaja Andmed',
    'bills' => 'Arved',
    'licenses' => 'Load ja litsensid',
    'properties' => 'Kinnisvara',
    'criminal_records' => 'Karistusregister',
    'characters' => 'Karakterid',
    'user_cars' => 'Kasutaja Autod',
    'medical_history' => 'Meditsiinilised andmed',
    'ems' => 'Kiirabi',
    'police' => 'Politsei',
    'application' => 'Avaldus',
    'police_job_application' => 'Politsei Töö Avaldus',
    'ems_job_application' => 'Kiirabi Töö Avaldus',
    'fines' => 'Trahvid',
    'application_data' => 'Taotluse andmed',
    'actions' => 'Tegevused',
    'car_sale_confirmation' => 'Auto Müügi Kinnitamine',
    'login' => 'Logi Sisse',
    'reset_password' => 'Parooli Lähtestamine',
    'whitelist_application_with_web_user_registration' => 'Whitelist Avaldus',
    'fill_criminal_record_for' => 'Täida Raport Isiku Kohta:',
    'money' => 'Raha',
    'update_profile' => 'Update Profile',
    'car_data' => 'Auto Andmed',
    'help' => 'Veebi lehel legend',
    'whitelist_application_info' => 'Whitelist avalduse info',
    'password_reset' => 'Parooli lähtestamine',
    'mechanic_new_report' => 'Uus Sissekanne',
    'mechanic_my_report' => 'Minu Sissekanded',
];
