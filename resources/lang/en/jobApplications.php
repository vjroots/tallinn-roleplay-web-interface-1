<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'age' => 'Age (IRL)',
    'false' => 'No',
    'true' => 'Yes',
    'discordName' => 'Discord name',
    'chitChat' => 'Where do you chit-chat with other EMS workers?',
    'situation1' => 'Describe what you will do in next situation. You arrive at the scene where victim lies in the '
        . 'middle of the road. What will you do with your car? What will you do with victim? How you resolve this '
        . 'situation?',
    'futurePlans' => 'Who you want to be in future?',
    'havePlayedRp' => 'How long have you played Role play?',
    'playingTimes' => 'How often you play TallinnRp?',
    'irlExperienceEms' => 'Do you have IRL experience in EMS job?',
    'sirensAllowed' => 'Are ambulance allowed to drive with sirens when they are not responding to call?',
    'criminalActions' => 'Is EMS worker allowed to do criminal actions?',
    'emsRpExperience' => 'Do you previous experience in EMS RP?',
    'numberOfJobGrades' => 'How many job grades are in EMS?',
    'whyYouWantToBeEms' => 'Why you want to be EMS?',
    'characterDescription' => 'Short description of your character',
    'question1' => 'Question 1: You are chasing criminal with car. Criminal stops the car, exits from it and starts '
        . 'running towards the forest. What will you do?',
    'question2' => 'Question 2: Two men are fighting in public place. Next moment one of the man takes out a knife. '
        . 'How will you act?',
    'microphone' => 'Do you have a microphone?',
    'trafficStop' => 'You are in traffic stop. Describe what questions will you ask, how you behave, what will you check, etc.',
    'otherServers' => 'Name other servers where you are playing and which one you are most active?',
    'irlExperiencePolice' => 'Do you have IRL experience in police job?',
    'whenYouUseGun' => 'Describe at least 2 situation when you can use firearm',
    'copInAnotherServer' => 'Are you active cop in another server',
    'previousExperience' => 'Do you have previous police RP experience? If yes, Name server(s) and how long',
    'whyYouWantToBePolice' => 'Why you want to be police?',
    'every_day' => 'Every day',
    'every_day_after_work_school' => 'Every Day after work/school',
    'only_weekends' => 'Only weekends',
    'cant_answer_depends_on_how_much_time_I_have_after_work_and_personal_life' => 'Can\'t answer, depends on how much time I have after work and personal life',
    'other_servers' => 'Other Server',
    'previous_experience' => 'Previous Experience',
    'describe_your_actions' => 'Describe your actions',
    'will_shoot_criminal_with_my_firearm' => 'Will shoot criminal with my firearm',
    'will_hit_criminal_with_a_car' => 'Will hit criminal with a car',
    'will_drive_as_close_as_possible_exit_a_car_and_start_chase_criminal_on_foot_If_possible_i_will_use_taser_to_stop_criminal' => 'Will drive as close as possible, exit a car and start chase criminal on foot. If possible, i will use taser to stop criminal',
    'will_run_next_to_the_criminal_and_cuff_him_her_while_he_she_is_still_running' => 'Will run next to the criminal and cuff him/her while he/she is still running',
    'will_stop_chasing_and_i_will_go_back_to_patrol' => 'Will stop chasing and I will go back to patrol',
    'will_shoot_him_because_why_not' => 'Will shoot him, because why not',
    'will_take_out_a_firearm_and_aim_it_to_man_who_holds_a_knife_i_will_demand_him_to_put_down_a_knife_and_raise_his_hands_up' => 'Will take out a firearm and aim it to man who holds a knife. I will demand him to put down a knife and raise his hands up',
    'will_run_back-to_my_car_and_hit_both_man_with_my_car' => 'Will run back to my car and hit both man with my car',
    'will_run_away_because_i_am_scared_of_my_life' => 'Will run away because I am scared of my life',
    'will_watch_a_fight_because_it_is_fun' => 'Will watch a fight because it is fun',
    'few_days' => 'Few days',
    'first_time' => 'First time',
    'one_month' => 'One month',
    'two_months' => 'Two months',
    'longer_then_two_months' => 'Longer then two months',
    'in_discord' => 'In Discord',
    'in_game' => 'In Game',
];
