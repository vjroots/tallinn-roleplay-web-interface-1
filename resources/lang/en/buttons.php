<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Buttons
    |--------------------------------------------------------------------------
    |
    | This file contains translations of buttons
    |
    */

    'search' => 'Search',
    'sell_car' => 'Sell Car',
    'confirm_sell' => 'Confirm Sell',
    'cancel_sell_request' => 'Cancel Sell Request',
    'car_is_not_stolen_any_more' => 'Car Is Not Stolen Any More',
    'back_to_previous_page' => 'Back to Previous Page',
    'delete' => 'Delete',
    'delete_user' => 'Delete User',
    'remove' => 'Remove',
    'edit' => 'Edit',
    'open' => 'Open',
    'edit_record' => 'Edit Record',
    'create_new_record' => 'Create a New Record',
    'create_record' => 'Create Record',
    'remove_tattoos' => 'Remove Tattoos',
    'add_new_fines' => 'Add New Fines',
    'edit_fine' => 'Edit Fine',
    'add_fine' => 'Add Fine',
    'accept' => 'Accept',
    'reject' => 'Reject',
    'hire' => 'Hire',
    'add_car_model_name' => 'Add Car Model Name',
    'add_new_update_insurance' => 'Add New / Update Insurance',
    'un_declare_car_as_wanted' => 'UnDeclare Car as Wanted',
    'declare_war_as_wanted' => 'Declare Car as Wanted',
    'un_declare_as_stolen' => 'UnDeclare as Stolen',
    'declare_car_as_stolen' => 'Declare Car as Stolen',
    'confiscate_car' => 'Confiscate Car',
    'confirm_name_change' => 'Confirm Name Change',
    'confirm' => 'Confirm',
    'un_ban_user_from_whitelist' => 'UnBan User From Whitelist',
    'ban_user_from_whitelist' => 'Ban User From Whitelist',
    'un_declare_as_fugitive' => 'UnDeclare as Fugitive',
    'declare_as_fugitive' => 'Declare as fugitive',
    'login' => 'Login',
    'send_password_reset_link' => 'Send Password Reset Link',
    'reset_password' => 'Reset Password',
    'register' => 'Register',
    'submit_application' => 'Submit Application',
    'continue' => 'Continue',
    'update_whitelist_application' => 'Update whitelist application',
];
